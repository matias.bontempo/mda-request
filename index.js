const request = require("request");
const fs = require('fs');

const options_single = require('./options-single');
const options_double = require('./options-double');

/* ---------------- */

const SINGLE_QUOTATION = false;
const TWO_QUOTATIONS = true;

const logger = {};
const path = 'log.csv';

let iterations = 10;

/* ---------------- */

const getData = async () => {
  const last = Date.now();
  const data = [];
  const times = {};

  if (SINGLE_QUOTATION) {
    return new Promise((resolve, reject) => {
      request(options_single, (error, response, body) => {
        console.log('----------');
        if (error) console.log('Err:', error);
        else {
          data.node = response && `S-${response.headers.node}`;
          data.statusCode = response && response.statusCode
          data.body = `${body.slice(0, 50)}...`;
          data.time = Date.now() - last;
        }
        resolve([data]);
      });
    });
  }

  if (TWO_QUOTATIONS) {

    console.log('----------');

    const responses = await Promise.all([
      new Promise((resolve, reject) => {
        request(options_single, (error, response, body) => {
          console.log(`N${response.headers.node}: ${Date.now() - last}`);
          resolve({ error, response, body, time: Date.now() - last });
        });
      }),
      new Promise((resolve, reject) => {
        request(options_single, (error, response, body) => {
          console.log(`N${response.headers.node}: ${Date.now() - last}`);
          resolve({ error, response, body, time: Date.now() - last });
        });
      }),
    ]);

    console.log('----------');

    const r = [ responses[0].response, responses[1].response ];
    const b = [ `${responses[0].body.slice(0, 50)}...`, `${responses[1].body.slice(0, 50)}...` ];
    const t = [ responses[0].time, responses[1].time ];

    return [
      {
        node: r[0] && `C-${r[0].headers.node}`,
        statusCode: r[0] && r[0].statusCode,
        body: b[0],
        time: t[0],
      },
      {
        node: r[1] && `C-${r[1].headers.node}`,
        statusCode: r[1] && r[1].statusCode,
        body: b[1],
        time: t[1],
      }
    ]
  };

  return new Promise((resolve, reject) => {
    request(options_double, (error, response, body) => {
      console.log('----------');
      if (error) console.log('Err:', error);
      else {
        data.node = response && `D-${response.headers.node}`;
        data.statusCode = response && response.statusCode
        data.body = `${body.slice(0, 50)}...`;
        data.time = Date.now() - last;
      }
      resolve([data]);
    });
  });

}

const parseCSV = (d) => {
  let str = '';
  Object.keys(d).forEach(key => str += `${d[key]};`);
  return str;
}

const newQuotation = async () => {
  iterations -= 1;
  const dataArr = await getData();
  dataArr.forEach((data) => {
    fs.appendFileSync(path, `${parseCSV(data)}\n`);
    logger[data.node] = logger[data.node] ? (logger[data.node] + data.time)/2 : data.time;
    console.log(data);
  });

  if (iterations) newQuotation();
  else {
    console.log(logger);
  }
}

/* ---------------- */

// Create log file if not exists
fs.writeFile(path, 'node;statusCode;body;duration;\n', { flag: 'wx' }, (err) => {
  if (err) fs.appendFileSync(path, `-;-;-;-;\n`);
  else console.log("Log file created!\n\n");
});

newQuotation();
