const request = require('request');
const options_single = require('./options-single.js');

console.log('Begin...\n\n');

request(options_single, (error, response, body) => {
    if (error) console.log('Err:', error);
    else console.log(response);
});